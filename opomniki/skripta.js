window.addEventListener('load', function() {
	//stran nalozena
	
	//Vnesi ime
	document.getElementById("prijavniGumb").addEventListener("click", function(){
		var ime = document.querySelector("#uporabnisko_ime").value;
		document.getElementById("uporabnik").innerHTML = ime;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
	});
	
	//Dodaj opomnik
	var dodajOpomnik = function(){
		var naziv = document.getElementById("naziv_opomnika").value;
		document.getElementById("naziv_opomnika").value = "";
		var cas = document.getElementById("cas_opomnika").value;
		document.getElementById("cas_opomnika").value = "";
		
		document.getElementById("opomniki").innerHTML += " \
			<div class='opomnik senca rob'> \
				<div class='naziv_opomnika'>" + naziv + "</div> \
				<div class='cas_opomnika'> Opomnik čez <span>" + cas + "</span> sekund.</div> \
			</div> \
		"
	}
	document.getElementById("dodajGumb").addEventListener("click", dodajOpomnik);

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			var parent = document.getElementById("opomniki");

			if(cas == 0){
				alert("Opomnik!\n\nZadolžitev "+opomnik.querySelector(".naziv_opomnika").innerHTML+" je potekla!");
				parent.removeChild(opomnik);
			}else{
				cas--;
				casovnik.innerHTML=cas;
			}
		}
	}
	setInterval(posodobiOpomnike, 1000);

});
